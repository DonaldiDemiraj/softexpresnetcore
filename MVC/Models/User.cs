﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;




namespace MVC.Models
{
    public class User
    {
        [Key]
        public int UseriD { get; set; }


        [Required(ErrorMessage = "Required*")]
        public string Emri { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Mbiemri { get; set; }

        [Required(ErrorMessage = "Required*")]
        public DateTime Datlindja { get; set; }

        [Required(ErrorMessage = "Required*")]
        [DataType(DataType.PhoneNumber)]
        public string Nr_Tel { get; set; }


        [Required(ErrorMessage = "Required*")]
        public string Gjinia { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Punesimi { get; set; }


        [Required(ErrorMessage = "Required*")]
        [Display(Name = "StatusiMartesor")]
        public string StatusiMartesor { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Vendlindja { get; set; }
    }
}
