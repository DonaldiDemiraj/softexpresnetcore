﻿using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC.Models;

namespace MVC.Controllers
{
    public class UserController : Controller
    {
        private readonly UserContext _Db;

        public UserController(UserContext Db)
        {
            _Db = Db;
        }

        //search
        public async Task<IActionResult> Search(string childname)
        {
            try
            {
                //var usr = await _Db.Users.FindAsync(id);
                if (String.IsNullOrWhiteSpace(childname))
                {
                    var dataContext = _Db.Users.Include(c => c.UseriD).Include(c => c.Emri);
                    return View(await dataContext.ToListAsync());
                }
                else
                {
                    var searchItems = await _Db.Users.Include(c => c.UseriD).Include(c => c.Emri).Where(s => s.Emri.Contains(childname)).ToListAsync();
                    return View(searchItems);
                }

                return RedirectToAction("UserList");
            }
            catch (Exception ex)
            {
                return RedirectToAction("UserList");
            }
            
        }


        public IActionResult UserList()
        {
            try
            {
                //var usrList = _Db.Users.ToList();
                var usrList = from a in _Db.Users
                              select new User
                              {
                                  UseriD = a.UseriD,
                                  Emri = a.Emri,
                                  Mbiemri = a.Mbiemri,
                                  Datlindja = a.Datlindja,
                                  Nr_Tel = a.Nr_Tel,
                                  Gjinia = a.Gjinia,
                                  Punesimi = a.Punesimi,
                                  StatusiMartesor = a.StatusiMartesor,
                                  Vendlindja = a.Vendlindja,
                              };

                return View(usrList);
            }
            catch (Exception ex)
            {
                return View();
            }
            
        }


        public IActionResult Create(User obj)
        {
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(User obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   if(obj.UseriD == 0)
                    { 
                        _Db.Users.Add(obj);
                        await _Db.SaveChangesAsync();
                    }
                   else
                    {
                        _Db.Entry(obj).State = EntityState.Modified;
                        await _Db.SaveChangesAsync();
                    }
                    return RedirectToAction("UserList");
                    
                }
                return View();
            }
            catch(Exception ex)
            {
                return RedirectToAction("UserList");
            }
        }


        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var usr = await _Db.Users.FindAsync(id);
                if(usr!=null)
                {
                    _Db.Users.Remove(usr);
                    await _Db.SaveChangesAsync();
                }

                return RedirectToAction("UserList");
            }
            catch(Exception ex)
            {
                return RedirectToAction("UserList");
            }
        }



        //private void loadDDL()
        //{
        //    try
        //    {
        //        List<StatusiMartesor> list = new List<StatusiMartesor>();
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //}
    }
}
